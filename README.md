# TYPO3 Extension `setdefaultauthor`
[![pipeline status](https://gitlab.com/typo3graf/developer-team/extensions/setdefaultauthor/badges/master/pipeline.svg)](https://gitlab.com/typo3graf/developer-team/extensions/setdefaultauthor/-/commits/master)
[![Latest Stable Version](https://poser.pugx.org/t3graf/setdefaultauthor/v/stable)](https://packagist.org/packages/t3graf/setdefaultauthor)
[![Latest Unstable Version](https://poser.pugx.org/t3graf/setdefaultauthor/v/unstable)](https://packagist.org/packages/t3graf/setdefaultauthor)
[![Total Downloads](https://poser.pugx.org/t3graf/setdefaultauthor/downloads)](https://packagist.org/packages/t3graf/setdefaultauthor)
[![License](https://poser.pugx.org/t3graf/setdefaultauthor/license)](https://packagist.org/packages/t3graf/setdefaultauthor)

> Defaults the author/email fields in pages to the info from the currently logged in user.

- **Gitlab Repository**: [https://gitlab.com/typo3graf/developer-team/extensions/setdefaultauthor](https://gitlab.com/typo3graf/developer-team/extensions/setdefaultauthor)
- **TYPO3 Extension Repository**: [https://extensions.typo3.org/extension/setdefaultauthor](https://extensions.typo3.org/extension/setdefaultauthor)
- **Found an issue?**: [https://gitlab.com/typo3graf/developer-team/extensions/setdefaultauthor/issues](https://gitlab.com/typo3graf/developer-team/extensions/setdefaultauthor/issues)
## 1. Introduction
### Features

* Based on extbase, implementing best practices from TYPO3 CMS
* Simple and fast installation
* No configuration needed

### Screenshots
#### Page properties metadata
![Page edit](https://cdn.typo3graf-media.de/typo3/setdefaultauthor/edit_page_tmb.png)

- [Fullsize screenshot](https://cdn.typo3graf-media.de/typo3/setdefaultauthor/edit_page_full.png)

## 2. Usage

### Installation
#### Installation using composer
The recommended way to install the extension is by using [Composer][2]. In your Composer based TYPO3 project root, just do `composer require t3graf/setdefaultauthor`.
#### Installation as extension from TYPO3 Extension Repository (TER)
Download and install the extension `setdefaultauthor` with the extension manager module.

## 6. Contribution
Please create an issue at [https://gitlab.com/typo3graf/developer-team/extensions/setdefaultauthor/issues](https://gitlab.com/typo3graf/developer-team/extensions/setdefaultauthor/issues).

**Please use Gittlab only for bug-reporting or feature-requests. For support use the TYPO3 community channels or contact us by email.**

## 7. Support

If you need privat or personal support, contact us by email on [support@t3graf-media.de](support@t3graf-media.de)

**Be aware that this support might not be free!**

[1]: https://docs.typo3.org/typo3cms/extensions/setdefaultauthor/
[2]: https://getcomposer.org
