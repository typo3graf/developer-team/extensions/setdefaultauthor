.. include:: ../Includes.txt



.. _installation:

============
Installation
============

The extension needs to be installed as any other extension of TYPO3 CMS:

#. Switch to the module "Extension Manager".

#. Get the extension

   #. **Get it from the Extension Manager:** Press the "Retrieve/Update"
      button and search for the extension key *setdefaultauthor* and import the
      extension from the repository.

   #. **Get it from typo3.org:** You can always get current version from
      `https://extensions.typo3.org/extension/setdefaultauthor/
      <https://extensions.typo3.org/extension/setdefaultauthor/>`_ by
      downloading either the t3x or zip version. Upload
      the file afterwards in the Extension Manager.

   #. **Use composer**: Use `composer require t3graf/setdefaultauthor`.

.. note::
   There is nothing else to do after installation.
