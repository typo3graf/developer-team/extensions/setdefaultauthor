<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "setdefaultauthor".
 *
 * Auto generated 14-04-2016 16:46
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = [
  'title' => 'Set default author/email',
  'description' => 'Defaults the author/email fields in pages and sys_notes to the info from the currently logged in user',
  'category' => 'be',
  'version' => '10.2.0',
  'state' => 'stable',
  'uploadfolder' => false,
  'createDirs' => '',
  'clearcacheonload' => true,
  'author' => 'Mike Tölle',
  'author_email' => 'kontakt@t3graf-media.de',
  'author_company' => 'T3graf media-agentur UG',
  'constraints' =>
  [
    'depends' =>
    [
      'typo3' => '10.4.0-10.4.99',
    ],
    'conflicts' =>
    [
    ],
    'suggests' =>
    [
    ],
  ],
];
