# 10.1.0

## TASK
- [TASK] Release v10.1 984a199
- [TASK] Followup 52fe4f4
- [TASK] Force composer to use 10.4 0f591ca

## BUGFIX
- [BUGFIX] Change for TYPO3 v10.4 the function to embed author and e-mail c1832fc

# 10.0.1

## BREAKING
- [!!!][TASK] Dropping TYPO3 v6 and v7 support 36d5f10

## TASK
- [TASK] Update README.md 6571658
- [TASK] Update README.md e150b49
- [TASK] Add sys_note handling d6e67f6
- [TASK] Update TYPO3 version to v10 48b108a
- [TASK] Change namespace to t3graf c4c4c7d
- [TASK] Add UnitTests 08ab83d
- [TASK] Add changelog for release 10.0.0 0430308
- [!!!][TASK] Dropping TYPO3 v6 and v7 support 36d5f10
- [TASK] Create CHANGELOG.md 018419b
- [TASK] Add composer.json a882fd6
- [TASK] Add extension files e2eca66
- [TASK] Add Gitlab configuration 8eaf04d
- [TASK] Add ddev development configuration ecb8cd6

## BUGFIX
- [BUGFIX] Yaml check removed from gitlab-ci.yaml be0c77c
- [BUGFIX] Disable TYPO3 setup in .gitlab-ci.yml 6a10377
- [BUGFIX] Enforce checkout with linux lf consistent over all plattforms fdcbf7b

## MISC
- Release v10.0.1 37572eb
- [DOCS] Write new documentation 2f7ca19
- Update some settings for documentation rendering 91489cd
- Update changelog 7864e69
- [DOCS] Add Basic Documentation 1687edd
- Initial commit f0ae9b1

